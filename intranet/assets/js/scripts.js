$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

    $("#megaMenu .is-sub").hover(function () {
            var id = this.id;
            if (id != "") {
                var subs = $("#megaMenu .sub");
                for (var i = 0; i < subs.length; i++) {
                    subs[i].setAttribute("hidden", true);
                }

                var actives = $("#megaMenu .submenu-active");
                for (var i = 0; i < actives.length; i++) {
                    if ($(this).parent().parent().attr("id") == $(actives[i]).parent().parent().attr("id"))
                        actives[i].classList.remove("submenu-active");
                    if (parseInt($(this).parent().parent().attr("id").split("sub")[1]) < parseInt($(actives[i]).parent().parent().attr("id").split("sub")[1]))
                        actives[i].classList.remove("submenu-active");
                }
                this.classList.add("submenu-active");

                var index = id.split(".");
                var subId = "";
                if (index.length > 1) {
                    subId = index[0];
                    document.getElementById(subId + ".0").removeAttribute("hidden", true);
                    for (var i = 1; i < index.length - 1; i++) {
                        subId = subId + "." + index[i];
                        var subsShow = document.getElementById(subId + ".0");
                        subsShow.removeAttribute("hidden", true);
                    }
                    if (document.getElementById(id + ".0")) {
                        var subsShow = document.getElementById(id + ".0");
                        subsShow.removeAttribute("hidden", true);
                    }

                } else {
                    var subsShow = document.getElementById(index + ".0");
                    subsShow.removeAttribute("hidden", true);
                }
            }
        },
        function () {

        }
    );

    $("#megaMenuRegiao .is-sub").hover(function () {
            var id = this.id;
            if (id != "") {
                var subs = $("#megaMenuRegiao .sub");
                for (var i = 0; i < subs.length; i++) {
                    subs[i].setAttribute("hidden", true);
                }

                var actives = $("#megaMenuRegiao .submenu-active");
                for (var i = 0; i < actives.length; i++) {
                    if ($(this).parent().parent().attr("id") == $(actives[i]).parent().parent().attr("id"))
                        actives[i].classList.remove("submenu-active");
                    if (parseInt($(this).parent().parent().attr("id").split("subRegiao")[1]) < parseInt($(actives[i]).parent().parent().attr("id").split("subRegiao")[1]))
                        actives[i].classList.remove("submenu-active");
                }
                this.classList.add("submenu-active");

                var index = id.split(".");
                var subId = "";
                if (index.length > 1) {
                    subId = index[0];
                    document.getElementById(subId + ".0").removeAttribute("hidden", true);
                    for (var i = 1; i < index.length - 1; i++) {
                        subId = subId + "." + index[i];
                        var subsShow = document.getElementById(subId + ".0");
                        subsShow.removeAttribute("hidden", true);
                    }
                    if (document.getElementById(id + ".0")) {
                        var subsShow = document.getElementById(id + ".0");
                        subsShow.removeAttribute("hidden", true);
                    }

                } else {
                    var subsShow = document.getElementById(index + ".0");
                    subsShow.removeAttribute("hidden", true);
                }
            }
        },
        function () {

        }
    );

    $('#navMenu').on('click', function () {
        if ($('#megaMenuRegiao').hasClass('open')) {
            $('#megaMenuRegiao').stop(true, true).slideUp("400");
            $('#megaMenuRegiao').toggleClass('open');
        }

        if (!$('#megaMenu').hasClass('open')) { 
            $('#megaMenu').stop(true, true).slideDown("400");
            $('#megaMenu').toggleClass('open');
            $('.dark-screen').fadeTo(200, 1);
            $('.dark-screen').toggleClass('open');
        }
    });
    $('#navMenuRegiao').on('click', function () {
        if ($('#megaMenu').hasClass('open')) {
            $('#megaMenu').stop(true, true).slideUp("400");
            $('#megaMenu').toggleClass('open');
        }

        if (!$('#megaMenuRegiao').hasClass('open')) {
            $('#megaMenuRegiao').stop(true, true).slideDown("400");
            $('#megaMenuRegiao').toggleClass('open');
            $('.dark-screen').fadeTo(200, 1);
            $('.dark-screen').toggleClass('open');
        }
    });
    $('.dark-screen').on('click', function () {
        if ($('#megaMenuRegiao').hasClass('open')) {
            $('#megaMenuRegiao').stop(true, true).slideUp("400");
            $('#megaMenuRegiao').toggleClass('open');
            $('.dark-screen').fadeTo(200, 0);
            $('.dark-screen').toggleClass('open');
        }

        if ($('#megaMenu').hasClass('open')) {
            $('#megaMenu').stop(true, true).slideUp("400");
            $('#megaMenu').toggleClass('open');
            $('.dark-screen').fadeTo(200, 0);
            $('.dark-screen').toggleClass('open');
        }
    });
});

$(window).scroll(function () {
    $("a.ancora-conteudo").removeClass('ancora-hidden');

    if ($(this).scrollTop() > 50) {
        $("#navConteudos").addClass("fixo");
    } else {
        $("#navConteudos").removeClass("fixo");
        $("a.ancora-conteudo").addClass('ancora-hidden');
    }
});

$(".search").mouseenter(function () {
    $(this)
        .toggleClass("open");
    $(".search-icon")
        .toggleClass("open");
});

$(".search").mouseleave(function () {
    $(this)
        .removeClass("open");
    $(".search-icon")
        .toggleClass("open");
});

function selectRegiao(regiao) {
    document.getElementById("noRegiao").innerHTML = regiao;
}

function invertButtons() {
    var emailBtn = document.getElementById("emailBtn");
    var compromissoBtn = document.getElementById("emailBtn");
    if (emailBtn.classList.contains('disabled')) {
        emailBtn.classList.remove('disabled')
        compromissoBtn.classList.add('disabled')
    } else {
        emailBtn.classList.add('disabled')
        compromissoBtn.classList.remove('disabled')
    }
}

function abrirMenu() {
    var menu = document.getElementById("megaMenu");
    menu.slideDown("400");
    menu.toggleClass('open');
}