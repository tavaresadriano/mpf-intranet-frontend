$(document).ready(function(){
    $('.form-check').each(function(){
        if($(this).find('input').prop("checked") == true) {
            $(this).find('.checkmark').css('background-color', $(this).find('.checkmark').css('color'));
        }
    });
    $('.form-check').find('input').change(function() {
        if($(this).prop("checked") == true) {
            $(this).parent().find('.checkmark').css('background-color', $(this).parent().find('.checkmark').css('color'));
        } else {
            $(this).parent().find('.checkmark').css('background-color', '#eee');            
        }
    });
});