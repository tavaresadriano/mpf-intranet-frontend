var instancias = [];
var contadorAcoes = 0;
var delaySearch = false;

$(document).ready(function(){    
    checkForm();
});

$('#acoes-filtro').change(function() {
    checkForm();
});

$('#acoes-search').keyup(function() {
    if(delaySearch == false) {
        delaySearch = true;
        setTimeout(function(){
            checkForm();
            delaySearch = false;
        }, 500);
    }
});

function checkForm() {
    console.log($('#acoes-search').val());
    $('.acoes-item').each(function(){
        instancias = $(this).attr('data-instancia');
        if($('#acoes-filtro').val().includes("instancia"+$(this).attr('data-instancia'))) {
            $(this).addClass("show");
            $(this).show();
        } else {
            $(this).removeClass("show");
            $(this).hide();
        }
        if($('#acoes-search').val() != "") {
            if($(this).text().toUpperCase().indexOf($('#acoes-search').val().toUpperCase()) != -1) {
                $(this).addClass("show");
                $(this).show();
            } else {
                $(this).removeClass("show");
                $(this).hide();
            }
        }
    });
    contadorAcoes = $('.acoes-item.show').length;
    $('#acoes-contador').text(contadorAcoes);
}