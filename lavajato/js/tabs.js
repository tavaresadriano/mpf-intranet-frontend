$(document).ready(function(){
    $('.nav-link').each(function(){
        if($(this).hasClass('active')) {
            if($($(this).attr('href')).find('.tab-text').length > 0) {
                $(this).css('background-color', $($(this).attr('href')).find('.tab-text').css('color'));
            }
        }
    });
    $('.nav-link').click(function(){
        if($($(this).attr('href')).find('.tab-text').length > 0) {
            $(this).css('background-color', $($(this).attr('href')).find('.tab-text').css('color'));
        }
        setTimeout(function(){
            $('.nav-link').each(function(){
                if($(this).hasClass('active')) {
                    if($($(this).attr('href')).find('.tab-text').length > 0) {
                        $(this).css('background-color', $($(this).attr('href')).find('.tab-text').css('color'));
                    }
                } else {
                    $(this).css('background-color', 'transparent');
                }
            });
        }, 50);
    });
});