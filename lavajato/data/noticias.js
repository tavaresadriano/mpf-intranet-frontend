var noticias = [
    {
        data: "07/03/2019",
        titulo: "Força-tarefa Lava Jato busca garantir que R$ 2,5 bilhões, frutos do acordo com a Petrobras, sejam usufruídos pela sociedade brasileira",
        link: "forca-tarefa-lava-jato-busca"
    },
    {
        data: "11/03/2019",
        titulo: "Lava Jato chega aos cinco anos em momento decisivo",
        link: "lava-jato-chega-aos-cinco"
    },
    {
        data: "12/03/2019",
        titulo: "Força-tarefa Lava Jato esclarece dúvidas sobre acordo com Petrobras",
        link: "forca-tarefa-lava-jato-esclarece"
    },
]